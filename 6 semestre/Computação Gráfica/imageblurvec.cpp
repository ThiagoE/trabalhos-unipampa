#include <fstream>
#include <iostream>
// #include <string>
#include <vector>
#include <climits>
#include <cstdlib>
#include <cmath>
#include <cstdio>
#include <cstring>

using namespace std;

struct pixel
{
    int r;
    int g;
    int b;
    pixel(int r, int g, int b);
    pixel();
    ~pixel();
    
};

class image
{
public:
    image();
    ~image();
    image(int nwidth, int nheight);
    void loadimage(string nombre);
    void saveimage(string nombre);
    void negativeimage(void);
    void blurimage(void);
    void putpixel(int x, int y, int red, int green, int blue);
    void drawcircle(int x0, int y0, int radius);
    int getred(int x, int y);
    int getgreen(int x, int y);
    int getblue(int x, int y);
    void drawline(int x, int y, int x1, int y2, pixel color);

private:
    int width;
    int height;
    int maxcolor;

    vector< vector<pixel> > buffer;

};

pixel::pixel(){
}
pixel::~pixel(){
}

pixel::pixel(int r, int g, int b){
  this->r = r;
  this->g = g;
  this->b = b;

}

image::image()
{
    buffer.resize(1920,vector<pixel> (1280));
}

image::image(int nwidth, int nheight)
{
    buffer.resize(nwidth,vector<pixel> (nheight));
}

image::~image()
{
}

int image::getred(int i, int j)
{
    return  buffer[i][j].r;
}
int image::getgreen(int i, int j)
{
    return  buffer[i][j].g;
}
int image::getblue(int i, int j)
{
    return  buffer[i][j].b;
}

void image::putpixel(int i,int j,int red=0,int green=0,int blue=0)
{
    buffer[i][j].r=red;
    buffer[i][j].g=green;
    buffer[i][j].b=blue;
}

void image::loadimage(string nombre)
{
    int red,green,blue;
    cout << "Arquivo de entrada "<< nombre <<endl;
    ifstream iFile(nombre.c_str());

    if (iFile==NULL)
    {
        cout << "Missing file!" << endl;
        exit(-1);
    }

    iFile.ignore(INT_MAX, '\n');
//    iFile.ignore(INT_MAX, '\n'); // REMOVED BECAUSE OF imagemagick-CONVERT NOT INSERTING COMMENT LINE IN IMAGE
// IF USING IRFANVIEW, UNCOMMENT LINE ABOVE

    iFile >> width;
    iFile >> height;
    iFile >> maxcolor;
    buffer.resize(width,vector<pixel> (height));
    cout << width << " " << height << endl << maxcolor << endl;

    for(int j=0; j<height; j++)
        for(int i=0; i<width; i++)
        {

            iFile >> red;
            iFile >> green;
            iFile >> blue;

            putpixel(i,j,red,green,blue);
        }

    iFile.close();
}
void image::saveimage(string nombre)
{
    ofstream oFile(nombre.c_str());

    if (oFile==NULL)
    {
        cout << "Missing file!" << endl;
        exit(0);
    }
    oFile << "P3\n" << width << " " << height << "\n" << maxcolor << "\n";


    for(int j=0; j<height; j++)
    {
        for(int i=0; i<width; i++)
        {
            oFile << getred(i,j) << " " << getgreen(i,j) << " " << getblue(i,j) << " ";
        }
        oFile << endl;
    }

    oFile.close();
}

void image::negativeimage(void)
{

    for(int j=0; j<height; j++)
        for(int i=0; i<width; i++)
            putpixel(i,j,maxcolor-getred(i,j),maxcolor-getgreen(i,j),maxcolor-getblue(i,j));

}

void image::drawline(int x1, int y1, int x2, int y2, pixel color){

  int i,dx,dy,sdx,sdy,dxabs,dyabs,x,y,px,py;

  dx=x2-x1;      /* the horizontal distance of the line */
  dy=y2-y1;      /* the vertical distance of the line */
  dxabs=abs(dx);
  dyabs=abs(dy);
  /*sdx=sgn(dx);//se for qlquer numero positivo, é salvo +1 caso contrário, -1
  sdy=sgn(dy);*/
  if(dx < 0)
    sdx = -1;
  else
    sdx=1;
  
  if(dy < 0)
    sdy=-1;
  else
    sdy=1;
  x=dyabs>>1;
  y=dxabs>>1;
  px=x1;
  py=y1;

    if (dxabs>=dyabs) /* the line is more horizontal than vertical */
  {
    for(i=0;i<dxabs;i++)
    {
      y+=dyabs;
      if (y>=dxabs)
      {
        y-=dxabs;
        py+=sdy;
      }
      px+=sdx;
      putpixel(px,py,color.r, color.g, color.b);
    }
  }
  else /* the line is more vertical than horizontal */
  {
    for(i=0;i<dyabs;i++)
    {
      x+=dxabs;
      if (x>=dyabs)
      {
        x-=dyabs;
        px+=sdx;
      }
      py+=sdy;
      putpixel(px,py,color.r,color.g, color.b);
    }
  }
}
void image::drawcircle(int x0, int y0, int radius)
{
    int x = radius;
    int y = 0;
    int err = 0;

    while (x >= y)
    {
        putpixel(x0 + x, y0 + y);
        putpixel(x0 + y, y0 + x);
        putpixel(x0 - y, y0 + x);
        putpixel(x0 - x, y0 + y);
        putpixel(x0 - x, y0 - y);
        putpixel(x0 - y, y0 - x);
        putpixel(x0 + y, y0 - x);
        putpixel(x0 + x, y0 - y);

        y += 1;
        err += 1 + 2*y;
        if (2*(err-x) + 1 > 0)
        {
            x -= 1;
            err += 1 - 2*x;
        }
    }
}

void image::blurimage(void)
{

    for(int j=1; j<(height-1); j++)
    {
        for(int i=1; i<(width-1); i++)
        {
            int red = 4*buffer[i][j].r+2*(buffer[i-1][j].r+buffer[i+1][j].r+buffer[i][j-1].r+buffer[i][j+1].r);
            red = red+ buffer[i-1][j-1].r+buffer[i+1][j+1].r+buffer[i+1][j-1].r+buffer[i-1][j+1].r;

            int green = 4*buffer[i][j].g+2*(buffer[i-1][j].g+buffer[i+1][j].g+buffer[i][j-1].g+buffer[i][j+1].g);
            green = green+ buffer[i-1][j-1].g+buffer[i+1][j+1].g+buffer[i+1][j-1].g+buffer[i-1][j+1].g;

            int blue = 4*buffer[i][j].b+2*(buffer[i-1][j].b+buffer[i+1][j].b+buffer[i][j-1].b+buffer[i][j+1].b);
            blue = blue + buffer[i-1][j-1].b+buffer[i+1][j+1].b+buffer[i+1][j-1].b+buffer[i-1][j+1].b;

            putpixel(i,j,red/16,green/16,blue/16);
        }
    }
}



int main(int argc, char **argv)
{

    if (argc<3)
    {
        cout << "insufficient arguments" << endl;
        exit(0);
    }

    image* fig1=new image();
    image* fig2=new image();

    fig1->loadimage(argv[1]);
    //fig1->negativeimage();
    
    fig1->drawline(200,110,200,10,pixel(252,0,0));
    fig1->drawline(50,10,200,10,pixel(252,0,0));
    fig1->drawline(50,10,20,80,pixel(252,0,0));
    fig1->drawline(20,80,170,80,pixel(252,0,0));
    fig1->drawline(20,80,20,180,pixel(252,0,0));
    fig1->drawline(20,180,170,180,pixel(252,0,0));
    fig1->drawline(170,80,170,180,pixel(252,0,0));
    fig1->drawline(170,80,200,10,pixel(252,0,0));
    fig1->drawline(170,180,200,110,pixel(252,0,0));
    fig1->drawline(20,180,50,110,pixel(252,0,0));
    fig1->drawline(50,110,200,110,pixel(252,0,0));
    fig1->drawline(50,10,50,110,pixel(252,0,0));
    fig1->drawcircle(100,150,20);
    fig1->saveimage(argv[2]);
    delete fig1;
    delete fig2;

    return 0;
}