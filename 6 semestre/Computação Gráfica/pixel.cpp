#include <fstream>
#include <iostream>
#include <string>
#include <climits>

using namespace std;

int main(void ){
    long int width, height, maxcolor;
    
    ifstream iFile("imagem.ppm");
    ofstream oFile("saida.ppm");
    ofstream oFileNeg("saidaNeg.ppm");
    ofstream oFileEspelhada("saidaEspelhada.ppm");
    //iFile.ignore(INT_MAX,'\n');
    iFile.ignore(INT_MAX,'\n');
    
    iFile >> width;
    iFile >> height;
    iFile >> maxcolor;


    struct pixel{
        long int r, g, b;

    }image[width][height];
    

    for (long int j = 0; j < height; j++){
        for (long int i = 0; i < width; i++){
            iFile >> image[i][j].r;
            iFile >> image[i][j].g;
            iFile >> image[i][j].b;
        }    
    }

    //normal
    oFile <<  "P3 \n" << width << " " << height << "\n" << maxcolor << "\n"; 
    for (long int j=0; j < height; j++){
        for (long int i = 0; i < width; i++){
            oFile << image[i][j].r << " " << image[i][j].g << " " << image[i][j].b << " ";
        }
        oFile << "\n";
    }

    //negativada
    oFileNeg <<  "P3 \n" << width << " " << height << "\n" << maxcolor << "\n"; 
    for (long int j=0; j < height; j++){
        for (long int i = 0; i < width; i++){
            oFileNeg << maxcolor - image[i][j].r << " " << maxcolor - image[i][j].g << " " << maxcolor - image[i][j].b << " ";
        }
        oFileNeg << "\n";
    }

     //normal
    oFile <<  "P3 \n" << width << " " << height << "\n" << maxcolor << "\n"; 
    for (long int j=height; j > 0; j--){
        for (long int i = 0; i < width; i++){
            oFileEspelhada << image[i][j].r << " " << image[i][j].g << " " << image[i][j].b << " ";
        }
        oFileEspelhada << "\n";
    }

    iFile.close();
    oFile.close();
    oFileNeg.close();
    oFileEspelhada.close();

    return 0;
}
//imagemagick convert arquivo.jpg -compress none saida.ppm
//imagem(negativada) invertida e espelhada até 600*640
// negativar -> r1 = maxcolor(q é255) - r