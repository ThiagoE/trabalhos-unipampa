#include <stdio.h>
#include <stdlib.h>
#include <math.h>

typedef struct{
	float x;
	float y;
} Point;

float PointDist(Point *point1, Point *point2){
	//printf("ponto 1: %f %f\n",point1->x,point1->y);
    //printf("ponto 2: %f %f\n",point2->x,point2->y);
    float dist;

    dist = sqrt(pow((point2->x - point1->x),2) + pow((point2->y - point1->y),2));
	return dist;

}

int PointEq(Point *point1,Point *point2){
	float dist;
	int i=2;
	dist = PointDist(point1,point2);
	if(dist < 0.000001){
		return 1;
	}else{
		return 0;
	}
}

int main(void){
	int i,iguais;
	float distancia;
	Point pontos[100];//100 structs na pilha 
	Point* ppontos[100];

	for(i = 0; i<100; i++){
		pontos[i].x = (float)i/100.0;
		pontos[i].y = (float)i/100.0;
	}

	for(i = 0; i<100; i++)
		ppontos[i] = (Point*)malloc(sizeof(Point));


	for(i = 0; i<100; i++){
		ppontos[i]->x = (float)i/100.0;
		ppontos[i]->y = (float)i/100;
	}

	distancia = PointDist(ppontos[3],ppontos[4]);
	iguais = PointEq(ppontos[3],ppontos[4]);
	printf("A distancia entre eles sao: %f \n", distancia);
	printf("Iguais? %d \n",iguais);
	
	for(i = 0; i < 100; i++){
		free(ppontos[i]);
		ppontos[i] = NULL;
	}

	/*
	for(i = 0; i < 100; i++){
		printf("x: %f", pontos[i].x);
		printf("y: %f \n",pontos[i].y);

	}*/

}